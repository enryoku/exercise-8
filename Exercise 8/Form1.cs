﻿// Mark W Paynter
// CST-117
// 05/12/2019
// Aiman Darwiche

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exercise_8
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        // this method is to take action once the CALCULATE button is clicked
        private void CalculateButton_Click(object sender, EventArgs e)
        {
            // I Mark Paynter attest that the work done on line 25 and from line 28 (this line) to line 62 not including line 50 are my work and also parts of the Form1 design but I don't know how to isolate what is only my work.
            // define variables needed to perform calculation
            int calFromFat;
            int calFromCarb;
            int gramsFat;
            int gramsCarb;
            // check to see if we can convert both text boxes
            if (Int32.TryParse(fatBox.Text, out gramsFat) && Int32.TryParse(carbBox.Text, out gramsCarb))
            {
                // call both external methods to calculate input values
                calFromFat = FatCalories(gramsFat);
                calFromCarb = CarbCalories(gramsCarb);
                // display results
                calcBox.Text = (calFromFat + calFromCarb).ToString();
            }
            else
            {
                // display error when conversion fails
                MessageBox.Show("Please enter a valid integer. I was not able to parse " + fatBox.Text + " or " + carbBox.Text + ".");
            }
            
            
        }
        // method to convert grams of fat to calories
        public int FatCalories(int input)
        {
            int caloriesFromFat = input * 9;
            return caloriesFromFat;
        }
        // method to convert grams of carbs to calories
        public int CarbCalories(int input)
        {
            int caloriesFromCarbs = input * 4;
            return caloriesFromCarbs;
        }
    }
}
