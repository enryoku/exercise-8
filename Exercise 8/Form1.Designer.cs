﻿namespace Exercise_8
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.calculateButton = new System.Windows.Forms.Button();
            this.inputFatLabel = new System.Windows.Forms.Label();
            this.inputCarbLabel = new System.Windows.Forms.Label();
            this.calcLabel = new System.Windows.Forms.Label();
            this.fatBox = new System.Windows.Forms.TextBox();
            this.carbBox = new System.Windows.Forms.TextBox();
            this.calcBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // calculateButton
            // 
            this.calculateButton.Location = new System.Drawing.Point(15, 117);
            this.calculateButton.Name = "calculateButton";
            this.calculateButton.Size = new System.Drawing.Size(256, 23);
            this.calculateButton.TabIndex = 0;
            this.calculateButton.Text = "CALCULATE !";
            this.calculateButton.UseVisualStyleBackColor = true;
            this.calculateButton.Click += new System.EventHandler(this.CalculateButton_Click);
            // 
            // inputFatLabel
            // 
            this.inputFatLabel.AutoSize = true;
            this.inputFatLabel.Location = new System.Drawing.Point(12, 12);
            this.inputFatLabel.Name = "inputFatLabel";
            this.inputFatLabel.Size = new System.Drawing.Size(107, 13);
            this.inputFatLabel.TabIndex = 1;
            this.inputFatLabel.Text = "Number of Fat Grams";
            // 
            // inputCarbLabel
            // 
            this.inputCarbLabel.AutoSize = true;
            this.inputCarbLabel.Location = new System.Drawing.Point(12, 45);
            this.inputCarbLabel.Name = "inputCarbLabel";
            this.inputCarbLabel.Size = new System.Drawing.Size(119, 13);
            this.inputCarbLabel.TabIndex = 2;
            this.inputCarbLabel.Text = "Number of Carbs Grams";
            // 
            // calcLabel
            // 
            this.calcLabel.AutoSize = true;
            this.calcLabel.Location = new System.Drawing.Point(12, 80);
            this.calcLabel.Name = "calcLabel";
            this.calcLabel.Size = new System.Drawing.Size(97, 13);
            this.calcLabel.TabIndex = 3;
            this.calcLabel.Text = "Calculated Calories";
            // 
            // fatBox
            // 
            this.fatBox.Location = new System.Drawing.Point(171, 12);
            this.fatBox.Name = "fatBox";
            this.fatBox.Size = new System.Drawing.Size(100, 20);
            this.fatBox.TabIndex = 4;
            // 
            // carbBox
            // 
            this.carbBox.Location = new System.Drawing.Point(171, 45);
            this.carbBox.Name = "carbBox";
            this.carbBox.Size = new System.Drawing.Size(100, 20);
            this.carbBox.TabIndex = 5;
            // 
            // calcBox
            // 
            this.calcBox.Location = new System.Drawing.Point(171, 80);
            this.calcBox.Name = "calcBox";
            this.calcBox.ReadOnly = true;
            this.calcBox.Size = new System.Drawing.Size(100, 20);
            this.calcBox.TabIndex = 6;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(283, 152);
            this.Controls.Add(this.calcBox);
            this.Controls.Add(this.carbBox);
            this.Controls.Add(this.fatBox);
            this.Controls.Add(this.calcLabel);
            this.Controls.Add(this.inputCarbLabel);
            this.Controls.Add(this.inputFatLabel);
            this.Controls.Add(this.calculateButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button calculateButton;
        private System.Windows.Forms.Label inputFatLabel;
        private System.Windows.Forms.Label inputCarbLabel;
        private System.Windows.Forms.Label calcLabel;
        private System.Windows.Forms.TextBox fatBox;
        private System.Windows.Forms.TextBox carbBox;
        private System.Windows.Forms.TextBox calcBox;
    }
}

